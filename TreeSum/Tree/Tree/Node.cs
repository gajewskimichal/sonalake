﻿namespace Tree.Tree
{
    public class Node
    {
        public int Value { get; private set; }

        public Node Left { get; private set; }
        public Node Right { get; private set; }

        public Node(int value)
        {
            Value = value;
        }

        public void SetLeftChildren(int value)
        {
            Left = new Node(value);
        }
        public void SetLeftChildren(Node node)
        {
            Left = node;
        }

        public void SetRightChildren(int value)
        {
            Right = new Node(value);
        }

        public void SetRightChildren(Node node)
        {
            Right = node;
        }
    }
}
