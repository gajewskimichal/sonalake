﻿namespace Tree.Tree
{
    public static class NodeExtensions
    {
        public static bool IsEven(this Node node)
        {
            return node.Value % 2 == 0;
        }
    }
}
