﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tree.Tree
{
    public class TreeBuilder
    {
        public BinaryTree Build(string path)
        {
            var fileContent = File.ReadAllLines(path);

            var nodesMatrix = new List<List<Node>>();

            for (int line = 0; line < fileContent.Count(); line++)
            {
                nodesMatrix.Add(new List<Node>());
                var valuesOnCurrentLevel = fileContent[line].Split(' ').Where(s => !String.IsNullOrEmpty(s));
                foreach (var value in valuesOnCurrentLevel)
                {
                    nodesMatrix.Last().Add(new Node(int.Parse(value)));
                }
            }

            for (int levelIndex = 0; levelIndex < nodesMatrix.Count(); levelIndex++)
            {
                var treeLevel = nodesMatrix[levelIndex];

                if(levelIndex < nodesMatrix.Count - 1)
                {
                    var nextTreeLevel = nodesMatrix[levelIndex + 1];

                    for (int nodePosition = 0; nodePosition < treeLevel.Count(); nodePosition++)
                    {
                        var currentNode = treeLevel[nodePosition];

                        currentNode.SetLeftChildren(nextTreeLevel[nodePosition]);
                        currentNode.SetRightChildren(nextTreeLevel[nodePosition + 1]);
                    }
                }
            }

            return new BinaryTree(nodesMatrix[0][0]);
        }
    }
}
