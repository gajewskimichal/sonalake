﻿namespace Tree.TreeMaxSumCalculator
{
    public class Result
    {
        public int? Value { get; set; }
        public string Path { get; set; }
    }
}
