﻿using System;
using Tree.Tree;

namespace Tree.TreeMaxSumCalculator
{
    public class TreeMaxSumCalculator
    {
        public Result Calculate(BinaryTree tree)
        {
            if (tree == null || tree.Root == null)
                throw new ArgumentException();

            var result = VisitNode(tree.Root, String.Empty);

            return result;
        }

        private Result VisitNode(Node root, string empty)
        {
            Result sumFromLeft = null;
            Result sumFromRight = null;
            var leftChild = root.Left;
            var rightChild = root.Right;
            bool isCurrentNodeEven = root.IsEven();

            var currentPath = String.IsNullOrEmpty(empty) ? root.Value.ToString() :  $"{empty} > {root.Value}";

            if (leftChild != null && (isCurrentNodeEven != leftChild.IsEven()))
                sumFromLeft = VisitNode(leftChild, currentPath);
            
            if (rightChild != null && (isCurrentNodeEven != rightChild.IsEven()))
                sumFromRight = VisitNode(rightChild, currentPath);

            Result valueToReturn = SelectMaxValue(sumFromLeft, sumFromRight);

            if (leftChild != null && rightChild != null && valueToReturn == null)
                return null;

            if (valueToReturn != null)
            {
                valueToReturn.Value += root.Value;
                return valueToReturn;
            }

            return new Result() { Value = root.Value, Path = currentPath }; ;
        }

        private static Result SelectMaxValue(Result sumFromLeft, Result sumFromRight)
        {
            Result valueToReturn = null;

            if (sumFromLeft != null && sumFromRight != null && sumFromRight.Value.HasValue && sumFromLeft.Value.HasValue)
            {
                valueToReturn = sumFromLeft.Value > sumFromRight.Value ? sumFromLeft : sumFromRight;
            }
            else if (sumFromRight != null &&sumFromRight.Value.HasValue)
                valueToReturn = sumFromRight;
            else if (sumFromLeft != null && sumFromLeft.Value.HasValue)
                valueToReturn = sumFromLeft;

            return valueToReturn;
        }
    }
}
