﻿using System;
using System.IO;
using System.Reflection;

namespace TreeTests.Helpers
{
    public class FileHelpers
    {
        public static string GetPath()
        {
            string current_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            UriBuilder uri = new UriBuilder(current_path);
            string path = Uri.UnescapeDataString(uri.Path);

            return Path.GetDirectoryName(path);
        }
    }
}
