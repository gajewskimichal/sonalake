﻿using NUnit.Framework;
using Tree.Tree;
using TreeTests.Helpers;

namespace TreeTests
{
    [TestFixture]
    public class TreeTests
    {
        [Test]
        public void binary_tree_has_correct_root_after_create()
        {
            Node root = new Node(1);

            var binaryTree = new BinaryTree(root);

            Assert.That(binaryTree.Root.Value, Is.EqualTo(1));
        }

        [Test]
        [Category("Integration")]
        public void tree_builded_from_sample_has_correct_nodes()
        {
            TreeBuilder builder = new TreeBuilder();
            var currentFolderPath = FileHelpers.GetPath();

            var tree = builder.Build($"{currentFolderPath}\\..\\sample.txt");

            Assert.That(tree.Root.Value, Is.EqualTo(215));
            Assert.That(tree.Root.Left.Left.Left.Left.Left.Value, Is.EqualTo(229));
            Assert.That(tree.Root.Right.Right.Left.Right.Left.Value, Is.EqualTo(835));
        }
    }
}
