﻿using NUnit.Framework;
using Tree.Tree;

namespace TreeTests
{
    [TestFixture]
    public class NodeTests
    {
        [Test]
        public void left_children_correctly_crated_after_SetLeftChildren_call()
        {
            Node node = new Node(1);
            node.SetLeftChildren(2);

            Assert.That(node.Left, Is.Not.Null);
            Assert.That(node.Left.Value, Is.EqualTo(2));

            Assert.That(node.Right, Is.Null);
        }

        [Test]
        public void right_children_correctly_crated_after_SetRightChildren_call()
        {
            Node node = new Node(1);
            node.SetRightChildren(2);

            Assert.That(node.Right, Is.Not.Null);
            Assert.That(node.Right.Value, Is.EqualTo(2));

            Assert.That(node.Left, Is.Null);
        }

        [Test]
        public void isEven_return_true_when_number_is_even()
        {
            Node node = new Node(2);

            Assert.That(node.IsEven(), Is.EqualTo(true));
        }

        [Test]
        public void isEven_return_false_when_number_is_odd()
        {
            Node node = new Node(1);

            Assert.That(node.IsEven(), Is.EqualTo(false));
        }
    }
}
