﻿using NUnit.Framework;
using Tree.Tree;
using Tree.TreeMaxSumCalculator;
using TreeTests.Helpers;

namespace TreeTests
{
    [TestFixture]
    class TreeMaxSumCalculatorTests
    {
        [Test]
        public void calculate_two_levels_max_without_even_odd_distinction()
        {
            BinaryTree tree = new BinaryTree(new Node(1));
            tree.Root.SetLeftChildren(2);
            tree.Root.SetRightChildren(4);

            TreeMaxSumCalculator calculator = new TreeMaxSumCalculator();
            var result = calculator.Calculate(tree);

            Assert.That(result.Value, Is.EqualTo(5));

            tree = new BinaryTree(new Node(1));
            tree.Root.SetLeftChildren(4);
            tree.Root.SetRightChildren(2);

            result = calculator.Calculate(tree);

            Assert.That(result.Value, Is.EqualTo(5));
        }

        [Test]
        public void calculate_max_with_even_even_seqention_existing()
        {
            BinaryTree tree = new BinaryTree(new Node(1));

            tree.Root.SetLeftChildren(9);
            tree.Root.SetRightChildren(2);

            TreeMaxSumCalculator calculator = new TreeMaxSumCalculator();
            var result = calculator.Calculate(tree);

            Assert.That(result.Value, Is.EqualTo(3));
        }

        [Test]
        public void path_which_dont_reach_bottom_should_be_skipped()
        {
            BinaryTree tree = new BinaryTree(new Node(1));

            tree.Root.SetLeftChildren(10);
            tree.Root.SetRightChildren(2);

            tree.Root.Left.SetLeftChildren(2);
            tree.Root.Left.SetRightChildren(2);

            tree.Root.Right.SetLeftChildren(1);
            tree.Root.Right.SetRightChildren(1);

            TreeMaxSumCalculator calculator = new TreeMaxSumCalculator();
            var result = calculator.Calculate(tree);

            Assert.That(result.Value, Is.EqualTo(4));
        }

        [Test]
        public void example_no_1_from_email()
        {
            BinaryTree tree = new BinaryTree(new Node(1));

            tree.Root.SetLeftChildren(8);
            tree.Root.SetRightChildren(9);

            tree.Root.Left.SetLeftChildren(1);
            tree.Root.Left.SetRightChildren(5);
            tree.Root.Right.SetLeftChildren(5);
            tree.Root.Right.SetRightChildren(9);

            tree.Root.Left.Left.SetLeftChildren(4);
            tree.Root.Left.Left.SetLeftChildren(5);
            tree.Root.Left.Right.SetLeftChildren(5);
            tree.Root.Left.Right.SetLeftChildren(2);


            tree.Root.Right.Left.SetLeftChildren(5);
            tree.Root.Right.Left.SetLeftChildren(2);
            tree.Root.Right.Right.SetLeftChildren(2);
            tree.Root.Right.Right.SetLeftChildren(3);

            TreeMaxSumCalculator calculator = new TreeMaxSumCalculator();
            var result = calculator.Calculate(tree);

            Assert.That(result.Value, Is.EqualTo(16));
        }

        [Test]
        [Category("Integration")]
        public void example_no_2_from_email()
        {
            TreeBuilder builder = new TreeBuilder();
            TreeMaxSumCalculator calculator = new TreeMaxSumCalculator();
            var currentFolderPath = FileHelpers.GetPath();

            var tree = builder.Build($"{currentFolderPath}\\..\\sample.txt");
            var maxSum = calculator.Calculate(tree);

            Assert.That(maxSum.Value, Is.GreaterThan(0));
        }
    }
}
